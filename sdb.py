import datetime
import urler
import urllib2
import neologin
import string
import time
import update
from sqlite3 import dbapi2 as sqlite

class s:
	connection = sqlite.connect('neoitems.sqlite')
	cursor = connection.cursor()


class opener():
 if 1 == 1:
	try:
		x = neologin.login()
	except:
		x = False
#	pass

def screenscrape(url,cont,urlextra):
	print "scraping " + url
	
	if opener.x != False:
		request = urllib2.Request(url)
		page = opener.x.open(request)
		contents = page.read()
		fullcontents=contents
		page.close
		offset = 0
		while cont in contents:
			offset += 30
			print 'Scraping '+ url + ' ' + str(offset / 30 + 1) 
			#contents = urler.copener(url + urlextra + str(offset), opener.x)
			if urlextra == '&lim=': contents = urler.copener(url + urlextra + str(offset +30), opener.x)
			elif urlextra == '&page=': contents = urler.copener(url + urlextra + str((offset / 30) + 1), opener.x)
			else: contents = urler.copener(url + urlextra + str(offset), opener.x)
			fullcontents += contents 
		
		return fullcontents
	else: 
		print "opener == false"
		return None
	
def parser(tags, contents):
	x=string.find(contents, tags[0]) + len(tags[0])
	y=string.find(contents[x:], tags[1]) + len(tags[1]) + x - 1
	tag=contents[x:y]
	return tag, contents[y:] 
		


def get_sdbparse(contents, itemdict):
	#itemdict ={}
	contents=contents[string.find(contents, 'Your Safety Deposit Box'):]		
	x = 0
	while 'back_to_inv[' in contents:
		x +=1
		imagetag=['<img src="http://images.neopets.com/items/','.']
		image, contents=parser(imagetag, contents)
		
		nametag=['<td align="left"><b>','<']
		name, contents=parser(nametag, contents)
		
		numbertag=['<td align="center"><b>','<']
		number, contents=parser(numbertag, contents)
		
		itemidtag=['back_to_inv[',']']		
		itemid, contents=parser(itemidtag, contents)
		
		s.cursor.execute('SELECT location FROM items WHERE neo_id=(?)', (itemid,))
		location = s.cursor.fetchone()
		if location: s.cursor.execute('UPDATE items SET location="SDB" WHERE neo_id=(?)', (itemid,))
		s.connection.commit()
		
		itemdict[itemid]={'image':image,'name':name,'number':number}
	#exit()
	return itemdict
		
def get_storeparse(contents, itemdict):
	#contents=contents[string.find(contents, "Aktux says 'Welcome!'"):]		
	while 'http://images.neopets.com/items/' in contents:
		nametag=["<td width=60 bgcolor='#ffffcc'><b>",'<']
		name, contents=parser(nametag, contents)
		if name == "ost_4'  value='5391'>": 
			#print contents
			exit()
		
		imagetag=["<img src='http://images.neopets.com/items/",'.']
		image, contents=parser(imagetag, contents)
		
		numbertag=['align=center><b>','<']
		number, contents=parser(numbertag, contents)
		number = int(number)
		
		itemidtag=["value='","'"]		
		itemid, contents=parser(itemidtag, contents)
		
		try: 
			if itemdict.has_key(itemid): number+=itemdict[itemid]["number"]
		except:
			print itemdict[itemid]
			exit()
		s.cursor.execute('SELECT location FROM items WHERE neo_id=(?)', (itemid,))
		location = s.cursor.fetchone()
#		if location[0]=='Store': s.cursor.execute('UPDATE items SET comment="HTS" WHERE neo_id=(?)', (itemid,))
#		el
		if location: s.cursor.execute('UPDATE items SET location="Store" WHERE neo_id=(?)', (itemid,))
		s.connection.commit() 
		itemdict[itemid]={'image':image,'name':name,'number':number}
	return itemdict

def get_neohomeparse(contents, itemdict):
	names = string.split(contents, 'market.phtml?type=wizard&string=')
#	print names[1]
#	exit()
	namedict = {}
	for i in names[1:]:
		name = string.split(i, "'>")[0]
#		print name
#		exit()
		if namedict.has_key(name): namedict[name] += 1
		else: namedict[name] = 1
		
	for item in itemdict: #awkward
		if itemdict[item].has_key('name') and namedict.has_key(itemdict[item]['name']): 
			itemdict[item]['number']+=namedict[itemdict[item]['name']]
			del namedict[itemdict[item]['name']]
	
	for name in namedict:
		s.cursor.execute('SELECT neo_id FROM items WHERE name = (?)', (name,))
		neo_id = s.cursor.fetchone()
		if neo_id[0] == 0: continue #awkward
#		print neo_id
#		itemdict[neo_id[0]]
		itemdict[neo_id[0]]={'number':namedict[name]}
		s.cursor.execute('UPDATE items SET location="Neohome" WHERE neo_id= (?)', (neo_id))
		s.connection.commit()
	return itemdict
		
def get_cardparse(contents, itemdict):
	names = string.split(contents, "border='0'><br><b>")
	namedict = {}
	for i in names[1:]:
		name = string.split(i, "</b></a></td>")[0]
		namedict[name] = string.split(string.split(i, "padding:2px;'><b>")[1], '</b></td>')[0]
		
	for item in itemdict: #awkward
		if itemdict[item].has_key('name') and namedict.has_key(itemdict[item]['name']): 
			itemdict[item]['number']+=namedict[itemdict[item]['name']]
			del namedict[itemdict[item]['name']]
	
	for name in namedict:	
		s.cursor.execute('SELECT neo_id FROM items WHERE name = (?)', (name,))
		neo_id = s.cursor.fetchone()
		try: 
			if not neo_id[0]: continue #awkward
		except:
			print "neoid not found: " + str(name) 
			continue #awkward
		itemdict[neo_id[0]]={'number':namedict[name]}
		s.cursor.execute('UPDATE items SET location="Deck" WHERE neo_id= (?)', (neo_id))
		s.connection.commit()
	return itemdict


def get_bdparse(contents, itemdict):
	#contents=contents[string.find(contents, "Aktux says 'Welcome!'"):]		
	while 'http://images.neopets.com/items/' in contents:
		itemidtag=["passPin(",","]		
		itemid, contents=parser(itemidtag, contents)
		
		imagetag=["<img src='http://images.neopets.com/items/",'.']
		image, contents=parser(imagetag, contents)
		
		nametag=["></a><br><b>",'<']
		name, contents=parser(nametag, contents)
		
		number = 1
		if itemdict.has_key(itemid): number+=itemdict[itemid]["number"]
		s.cursor.execute('UPDATE items SET location="BD" WHERE neo_id=(?)', (itemid,))
		s.connection.commit() 
		itemdict[itemid]={'image':image,'name':name,'number':number}
	return itemdict

def get_closetparse(contents, itemdict):
	#itemdict ={}
#	contents=contents[string.find(contents, 'Your Safety Deposit Box'):]		
	x = 0
	while 'http://images.neopets.com/items/' in contents:
		x +=1
		imagetag=["<img src='http://images.neopets.com/items/",'.']
		image, contents=parser(imagetag, contents)
		
		nametag=['<td><b>','<']
		name, contents=parser(nametag, contents)
		
		typetag=["<td align='center'><b>",'<']
		number, contents=parser(typetag, contents)
		
		
		numbertag=["<td align='center'><b>",'<']
		number, contents=parser(numbertag, contents)
		
		itemidtag=["name='", "'"]		
		itemid, contents=parser(itemidtag, contents)
		
		s.cursor.execute('SELECT location FROM items WHERE neo_id=(?)', (itemid,))
		location = s.cursor.fetchone()
		if location: s.cursor.execute('UPDATE items SET location="SDB" WHERE neo_id=(?)', (itemid,))
		s.connection.commit()
		
		itemdict[itemid]={'image':image,'name':name,'number':number}
	#exit()
	return itemdict

def get_shedparse(contents, itemdict):
	x = 0
	while 'http://images.neopets.com/items/' in contents:
		x +=1
		imagetag=["<img src='http://images.neopets.com/items/",'.']
		image, contents=parser(imagetag, contents)
		
		nametag=['<td><strong>','<']
		name, contents=parser(nametag, contents)
		
		typetag=["<td align='center'><strong>",'<']
		number, contents=parser(typetag, contents)
		
		
		numbertag=["<td align='center'><strong>",'<']
		number, contents=parser(numbertag, contents)
		
		itemidtag=["name='", "'"]		
		itemid, contents=parser(itemidtag, contents)
		
		s.cursor.execute('SELECT location FROM items WHERE neo_id=(?)', (itemid,))
		location = s.cursor.fetchone()
		if location: s.cursor.execute('UPDATE items SET location="Shed" WHERE neo_id=(?)', (itemid,))
		s.connection.commit()
		
		itemdict[itemid]={'image':image,'name':name,'number':int(number)}
	#exit()
	return itemdict

def get_galleryparse(contents, itemdict):
	neo_ids = string.split(contents, "remove_arr[")
	id_list = []
	for i in neo_ids[1:]:
		neo_id = string.split(i, "]")[0]
		id_list.append(neo_id)
	numbers = string.split(contents, "Qty:")
	number_list = []
	for i in numbers[1:]:
		number = string.split(i, "<")[0]
		number_list.append(number)
	
	for i in xrange(len(id_list)):
		if itemdict.has_key(id_list[i]): itemdict[id_list[i]]['number']+=number_list[i]
		else: itemdict[id_list[i]]={'number':number_list[i]}
		s.cursor.execute('UPDATE items SET location="Gallery" WHERE neo_id= (?)', (id_list[i],))
	s.connection.commit()
	return itemdict

def get_intdate(olddate):
	year = str(olddate)[:4]
	month = str(olddate)[5:7]
	day = str(olddate)[8:10]
	truedate = int(year+month+day)
	return truedate


def update_db(itemdict):
	if len(itemdict) == 0:exit()
	s.cursor.execute('SELECT neo_id, date_bought, number FROM items WHERE number > 0')
	
	sdbdict = {}
	for row in s.cursor: 
		sdbdict[row[0]]=row[1], row[2]
	
	for item in sdbdict:
		if str(item) not in itemdict:
			s.cursor.execute('UPDATE items SET number=0 WHERE neo_id=(?)',(item,)) 
			s.cursor.execute('UPDATE items SET date_bought=null WHERE neo_id=(?)',(item,)) 
			s.cursor.execute('UPDATE items SET email=0 WHERE neo_id=(?)',(item,))
		
	x = 0
	today=get_intdate(datetime.date.today())
	
	print today
	for item in itemdict:
		x += 1
		print str(x) + ':' + str(len(itemdict))
		s.cursor.execute('SELECT date_bought FROM items WHERE neo_id=(?)', (item,)) #awkward
		rowlist = s.cursor.fetchone()
		if not rowlist: #no neo_id
#			print item
#			print rowlist
			try:codex_id = update.get_codex_id(itemdict[item]["name"],itemdict[item]["image"])
			except:
				print item, itemdict[item]
				continue
			if codex_id == 0: codex_id = -1 #awkward
				
			s.cursor.execute('SELECT * FROM items WHERE codex_id=(?)', (codex_id,))
			rowlist = s.cursor.fetchone()
			if not rowlist or codex_id == -1:	#no codex_id
#				print "not in db"
		#neo_id int, name text, codex_id int, img text, number int, tracking int, comment text, date_bought int, date_checked int, email int
				s.cursor.execute('INSERT INTO items VALUES (?,?,?,?,?,?,?,?,?,?,null)', (item, itemdict[item]["name"], codex_id, itemdict[item]["image"], itemdict[item]["number"], 0, '', today, 0, 0))	
			else: #has codex_id
#				print "in db"
				s.cursor.execute('UPDATE items SET neo_id=(?) WHERE codex_id=(?)', (item, codex_id))
				s.cursor.execute('UPDATE items SET number=(?) WHERE codex_id=(?)', (itemdict[item]["number"], codex_id))
				s.cursor.execute('UPDATE items SET date_bought=(?) WHERE codex_id=(?)', (today, codex_id))
				s.cursor.execute('UPDATE price_hist SET neo_id=(?) WHERE codex_id=(?)', (item, codex_id))
			s.connection.commit()
#			exit()
		else:	#has neo_id
			print "updating: " + str(item)		
			s.cursor.execute('UPDATE items SET number=(?) WHERE neo_id=(?)',(itemdict[item]["number"], item)) 
#			if rowlist[0] == 0 or rowlist[0]==None: s.cursor.execute('UPDATE items SET date_bought=(?) WHERE neo_id=(?)',(today, item)) 
			#s.cursor.execute('INSERT INTO items VALUES (?,?,?,?,?,?,?,?,?,?)', (item, itemdict[item]["name"], 0, itemdict[item]["image"], itemdict[item]["number"], 0, '', date_bought, 0, 0))
	s.connection.commit()
	
def get_scrape():
	opener = login.login()
	sdbcontents = ""
	return sdbcontents, storecontents
	storecontent = screenscrape('http://www.neopets.com/market.phtml?type=your&order_by=price',"name='subbynext' value='Next 30'>", '&lim=')

def program():
	if 1 == 1:
		storepage = screenscrape('http://www.neopets.com/market.phtml?type=your&order_by=price',"name='subbynext' value='Next 30'>",'&lim=')
		newfile = open('tempstore', 'w')
		newfile.write(storepage)
		newfile.close
		sdbpage = screenscrape('http://www.neopets.com/safetydeposit.phtml','<b>Next <span class="pointer">&raquo</span></b></a></td>', '?category=0&obj_name=&offset=')
		newfile = open('tempsdb', 'w')
		newfile.write(sdbpage)
		newfile.close
		neohomepage = screenscrape('http://www.neopets.com/neohome.phtml?type=item_list', 'gibberish', 'gibberish')
		newfile = open('tempneohome', 'w')
		newfile.write(neohomepage)
		newfile.close
		cardpage = screenscrape('http://www.neopets.com/games/neodeck/index.phtml?owner=REDACTED', 'gibberish', 'gibberish')
		newfile = open('tempcard', 'w')
		newfile.write(cardpage)
		newfile.close	
		bdpage = screenscrape('http://www.neopets.com/battledome/battledome.phtml?type=equip', 'gibberish', 'gibberish')
		newfile = open('tempbd', 'w')
		newfile.write(bdpage)
		newfile.close	
		closetpage = screenscrape('http://www.neopets.com/closet.phtml','<b>Next <span class="pointer">&raquo</span></b></a></td>', '?obj_name=&category=0&per_page=30&page=')
		newfile = open('tempcloset', 'w')
		newfile.write(closetpage)
		newfile.close
		shedpage = screenscrape('http://www.neopets.com/neohome/shed','<b>Next <span class="pointer">&raquo</span></b></a></td>', '?obj_name=&category=0&per_page=30&page=')
		newfile = open('tempshed', 'w')
		newfile.write(shedpage)
		newfile.close	
		gallerypage = screenscrape('http://www.neopets.com/gallery/index.phtml?dowhat=remove&view=all','gibberish', 'gibberish')
		newfile = open('tempgallery', 'w')
		newfile.write(gallerypage)
		newfile.close
		
	
	newfile = open('tempstore', 'r')
	storepage = newfile.read()
	newfile.close

	newfile = open('tempsdb', 'r')
	sdbpage = newfile.read()
	newfile.close
	
	newfile = open('tempneohome', 'r')
	neohomepage = newfile.read()
	newfile.close
	
	newfile = open('tempcard', 'r')
	cardpage = newfile.read()
	newfile.close
	
	newfile = open('tempbd', 'r')
	bdpage = newfile.read()
	newfile.close
	
	newfile = open('tempcloset', 'r')
	closetpage = newfile.read()
	newfile.close

	newfile = open('tempshed', 'r')
	shedpage = newfile.read()
	newfile.close

	newfile = open('tempgallery', 'r')
	gallerypage = newfile.read()
	newfile.close
	
	itemdict = {}
	print "galleryparse"
	itemdict = get_galleryparse(gallerypage, itemdict)
#	exit()
	print "shedparse"
	itemdict = get_shedparse(shedpage, itemdict)
	print "closetparse"
	itemdict = get_closetparse(closetpage, itemdict)
	print "bdparse"
	itemdict = get_bdparse(bdpage, itemdict)
	print "cardparse"
	itemdict = get_cardparse(cardpage, itemdict)
	print "neohomeparse"
	itemdict = get_neohomeparse(neohomepage, itemdict)
	print "storeparse"
	itemdict = get_storeparse(storepage, itemdict)
	print "sdbparse"
	itemdict = get_sdbparse(sdbpage, itemdict)
	print "update"
	update_db(itemdict)
	return 1
	#get current prices

	#output

def main():
	try: program()
	except:
		print "sdb didn't work", sys.exc_info()[0] 
		return None 
	return "happy"

if __name__ == '__main__':
	program()
