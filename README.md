# README

There is an online game I used to play in college called Neopets. Neopets has a currency called neopoints (np) you would earn playing games, etc. And then spend and that shops, buying virtual items.
There are about 50 neopets separate shops selling different items. There are tens of thousands of items. Each shop would restock at random times, random items.
One way to earn np is to buy rare items at the shop, and sell it in your own shop. However some items, for varying reasons, are way overpriced in the neopets shops, some underpriced. Underpriced items sell very, very fast, and you have to be very quick get the good ones.
This program (written in python) keeps a database (sqlite) of the current prices in the user shops, scraped from neopets itself, and another unaffiliated site called neocodex that kept its own database. 
Then when the user goes to the neopets shop, the scripts check the database and highlight what is actually worth buying. This uses javascript.
Also these prices change over time. Some almost creating a sine wave from month to month. During a war, battle items skyrocket in price, and settle back down when the war is over.
The program goes online, check what the user currently has in the 'safety deposit box' and informs the user when it was time to sell. it even sends a daily email with updates and current worth.

unfortunately, i got bored once the program was made.

keep in mind this program was designed for personal use and is very 'hacky'

