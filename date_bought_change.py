from sqlite3 import dbapi2 as sqlite

class s:
	connection = sqlite.connect('neoitems.sqlite')
	cursor = connection.cursor()

print "input codex_id or name"
toget = raw_input()
item={}
try: 
	codex_id = int(toget)
	s.cursor.execute('select name, date_bought, number from items where codex_id = (?)', (codex_id,))
	row = s.cursor.fetchone()
	item[codex_id]=[row[0], row[1], row[2]]

except:
	name = toget
	s.cursor.execute('select name, codex_id, date_bought, number from items where name like (?)', ('%'+name+'%',))
	rows = s.cursor.fetchall()
	if not rows:
		print "not in db"
		exit()
	for row in rows:
		codex_id = row[1]
		item[row[1]]=[row[0], row[2], row[3]]
	
#print item
#exit()

for i in item:
	if item[i][2] < 1: 
		print str(item[i]) + ' not in SDB'
		continue
	print str(item[i]) + ' enter new date: (0 = next item)'
	newdate=raw_input()
	if newdate == '0': continue
	if len(newdate) != 8: 
		print 'wrong length'
		exit()
	try: newdate = int(newdate)
	except:
		print 'not int'
		exit()
	s.cursor.execute('update items set date_bought=(?) where codex_id=(?)', (newdate, i))
	s.connection.commit()
	print 'success!'
	exit()
