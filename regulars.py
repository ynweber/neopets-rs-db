#get date list
#for each date 
#	get 6 month average for each year
#	if every year shows + or - 50:
#		print codex
#		print date \t avg

import report, datetime, sensors, time, os
from sqlite3 import dbapi2 as sqlite

class s:
	connection = sqlite.connect('neoitems.sqlite')
	cursor = connection.cursor()

def get_avg(dateslist, pricedict, current):
#	print dateslist
#	totalist =[]
##	for date in dateslist:
##		totalist.append(pricedict[date])
#	print sum(dateslist)
#	print len(dateslist)
	average = float(sum(dateslist)) / len(dateslist)
	
	if current == 0: average = 0
	else: average = int((average-current)/float(current)*100*-1)
	
	return average


def get_truedate(date):
	date = str(date)
#	print date
#	year = int(date[:4])
#	print year
#	month = int(date[4:6])
#	day = int(date[7:8])
#	print day
	date = datetime.date(int(date[:4]),int(date[4:6]),int(date[6:8]))
	return date
	
def get_avgs(dateslist, current, today): #awkward
	six_month_list = []
#	thirty_day_list = []
#	seven_day_list = []
	
#	sevendaysago = get_intdate(datetime.date.today() - datetime.timedelta(days=7))
#	thirtydaysago = get_intdate(datetime.date.today() - datetime.timedelta(days=30))
	sixmonthsago  = report.get_intdate(get_truedate(today) + datetime.timedelta(days=30))#180))
	for date in dateslist:
		if date[0] > sixmonthsago: break
		if date[0] <= today: continue
		six_month_list.append(date[1])
#		if date < thirtydaysago:
#			
#			continue		

#		if date < sevendaysago:
#			thirty_day_list.append(date)
#			continue
#		seven_day_list.append(date)
	if len(six_month_list) > 0: sixmonth = get_avg(six_month_list, dateslist, current)
	else: sixmonth = 0
#	if len(thirty_day_list) > 0: thirtyday = get_avg(thirty_day_list, pricedict, current)
#	else: thirtyday = 0
#	if len(seven_day_list) > 0: sevenday = get_avg(seven_day_list, pricedict, current)
#	else: sevenday = 0
	
	return sixmonth#, thirtyday, sevenday


def dates(codex_id):
#	firstime = time.time()
	s.cursor.execute('SELECT date, price FROM price_hist WHERE codex_id=(?)', (codex_id,))
#	secondtime = time.time()
#	for row in s.cursor:
#		datesdict[row[0]]=row[1]
	dateslist = s.cursor.fetchall()
#	if lambda date: date[0] == 20090805, dateslist: print "happy"
	selldict={}
	sellmonthdict={}
	buydict={}
	buymonthdict={}
#	print secondtime - firstime
	
	x=0
	freq = 200
	for date in dateslist:
		x+=1
		if x % freq == 0: freq = sensors.check(freq)
		if date[1] < 1000: continue

		avg = get_avgs(dateslist, date[1], date[0]) 
		testdate = str(date[0])
		month = testdate[4:6]
		year = testdate[0:4]
		
		if not selldict.has_key(year): selldict[year]=[]
		if avg > 35:
			if month not in selldict[year]:
				selldict[year].append(month)
				if not sellmonthdict.has_key(month): sellmonthdict[month]=0
				sellmonthdict[month]+=1 

		if not buydict.has_key(year): buydict[year]=[]	
		if avg < -10:
			if month not in buydict[year]:
				buydict[year].append(month)
				if not buymonthdict.has_key(month): buymonthdict[month]=0
				buymonthdict[month]+=1 

#	thirdtime = time.time()
#	print thirdtime - secondtime
	selllist=[]
	for month in sellmonthdict:
		if sellmonthdict[month] > 1: selllist.append(month) 
		
	buylist=[]
	for month in buymonthdict:
		if buymonthdict[month] > 1: buylist.append(month)
#	fourthtime = time.time()
#	print fourthtime - thirdtime

	if len(selllist) == 0: return# and len(buylist) == 0: return
	toprint = str(codex_id) + ": sell:" + str(selllist) + " buy:" + str(buylist)

	if os.path.isfile('regularsoutput'):newfile = open('regularsoutput', 'a')
	else:newfile = open('regularsoutput', 'w')
	
	newfile.write(toprint+'\n')
	newfile.close	
	
	
s.cursor.execute('SELECT codex_id FROM items')
codex_list = s.cursor.fetchall()
x=0
freq=200
for codex_id in codex_list:
	x += 1
	if x % freq == 0: freq = sensors.check(freq)
	dates(codex_id[0])
#	dates(416)
#	exit()
